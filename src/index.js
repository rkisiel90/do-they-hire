const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  // const browser = await puppeteer.launch({ headless: false });

  const page = await browser.newPage();
  await page.setViewport({
    height: 1000,
    width: 1500
  });


  // navigate to homepage
  await page.goto('https://x-team.com/', {
    waitUntil: 'networkidle0'
  });

  // locate footer link(element "Remote Programming Jobs")
  const footerLinkselector = '.footer__item .footer__link'

  const footerLink = await page.evaluate(footerLinkselector => {
    const links = Array.from(document.querySelectorAll(footerLinkselector));
    const results = links.filter(link => link.innerText === 'Remote Programming Jobs');
    return (results[0]) ? results[0].href : '';
  }, footerLinkselector);

  if(!footerLink) {
    console.log(`They do not hire :( (Can't find a link)`);
    await browser.close();
    return;
  }

  // Lets go to the Remote jobs!
  await page.goto(footerLink, {
    waitUntil: 'networkidle0'
  });

  // locate job link(element "Remote Frontend Developer Jobs")
  const FrontendDevLinkSelector = '.assemble__contents__item a';

  const FrontendDevLink = await page.evaluate(FrontendDevLinkSelector => {
    const links = Array.from(document.querySelectorAll(FrontendDevLinkSelector));
    const results = links.filter(link => link.text === 'Remote Frontend Developer Jobs');
    return (results[0]) ? results[0].href : '';
  }, FrontendDevLinkSelector);

  if (!FrontendDevLink) {
    console.log('There is no job for a Frontend Developer :(')
    await browser.close();
    return;
  }

  // Lets go to the Frontend Developer job!
  await page.goto(FrontendDevLink, {
    waitUntil: 'networkidle0'
  });

  // Type into application form
  await page.type('input.form__input[name=name]', 'Robert');
  await page.type('input.form__input[name=last_name]', 'Kisiel');
  await page.type('input.form__input[name=email]', 'rkisiel90@gmail.com');

  console.log('Yes, they hire! We can apply. :) ');

  // await page.click('[name="submit_form"]') // spam 😈😈😈😈

  // Take a screenshot of filled form
  await page.screenshot({ path: 'dist/filled.png' });

  // We are done here :)
  await browser.close();
})();