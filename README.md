# do-they-hire

Sample of code where I utilize [puppeteer - Headless Chrome Node API](https://github.com/GoogleChrome/puppeteer) to determine if X-team hires Frontend Developers :)

## Requirements

TL;DR: use Node v7.6.0 or greater

*Puppeteer requires at least Node v6.4.0, but the examples below use async/await which is only supported in Node v7.6.0 or greater.*

*I get used to manage my Node versions with [nvm](https://github.com/creationix/nvm) what I can highly recommend.*


## Getting started

```shell
yarn install
```

or

```shell
npm install
```

## Usage

```shell
node src/index.js
```